class VisibleApp extends React.Component{

    constructor(props){
        super(props);
        this.handleToggleVisibility = this.handleToggleVisibility.bind(this);

        this.state = {
            visibility: false
        };
    }

    handleToggleVisibility(){
        this.setState((prevState) => {
            return{
                visibility: !prevState.visibility
            };
        });

    }

    render(){
        return(
            <div>
                <h1>Visibility Toggle</h1>
                <button onClick={this.handleToggleVisibility}>
                {this.state.visibility ? "Hide Details" : "Show Details"}
                </button>
                {this.state.visibility && <p>Show the details here</p>}
            </div>
        );
    }
}

ReactDOM.render(<VisibleApp />, document.getElementById("app"));


// let buttontxt = 'Show Details';
// const txt = 'hey these are some details you can see!';
// let showtxt = false;

// const toggleBlock = () => {
//     showtxt = !showtxt;
    
//     if(showtxt){
//         buttontxt = 'Hide Details';
//     }else{
//         buttontxt = 'Show Details';
//     }

//     renderApp();
// };


// const appRoot = document.getElementById("app");

// const renderApp = () => {
//     const template = (
//         <div>
//             <h1>Visibility Toggle</h1>
//             <button onClick={toggleBlock}>{buttontxt}</button>
//             {showtxt && <p>{txt}</p>}
//         </div>
//     );

//     ReactDOM.render(template, appRoot);
// };

// renderApp();