console.log("app js is running")

const obj = {
    title: 'Indecision App',
    subtitle: 'This is a sample Subtitle2',
    options: []
};


const onFormSubmit = (e) => {
    e.preventDefault();
    const option = e.target.elements.option.value;

    if(option){
        obj.options.push(option);
        e.target.elements.option.value = "";
    }

    renderApp();
};

const removeAll = () => {
    console.log(obj.options.length);
    if(obj.options.length > 0){
        obj.options = [];
    }

    renderApp();
};

const onMakeDecision = () => {
    const randomNum = Math.floor(Math.random() * obj.options.length);
    const option = obj.options[randomNum];
    alert(option);
};

const appRoot = document.getElementById("app");

const renderApp = () => {
    const template = ( 
        <div>
            <h1>{obj.title}</h1>
            {obj.subtitle && <p>{obj.subtitle}</p>}    
            {obj.options.length > 0 ? "Here are your options" : "No options" }    
            
            <button disabled={obj.options.length === 0} onClick={onMakeDecision}>What should I do?</button>

            <button onClick={removeAll}>Remove All</button>
    
            <ol>
                {
                    obj.options.map((opt) =>  <li key={opt}>{opt}</li>)
                }
            </ol> 
    
            <form onSubmit={onFormSubmit}>
                <input type="text" name="option" />
                <button>Add Option</button>    
            </form>
    
        </div>
    );

    ReactDOM.render(template, appRoot);
};


renderApp();