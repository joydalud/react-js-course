var nameVar = 'Joy';
var nameVar = 'Majoy';
console.log('name: ',nameVar);

let nameLet = 'Jen';
nameLet = 'Julie';
console.log('namelet', nameLet);

const nameConst = 'Frank';
console.log('nameConst',nameConst);

function getPetName() {
    var petName = "Hal";
    return petName;
}

//block scoping
const fullName = 'Joy Dalud';
let firstName;

if(fullName){
    firstName = fullName.split(' ')[0];
    console.log(firstName);
}
console.log(firstName);

